<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Orderitem_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get orderitem by Id
     */
    function get_orderitem($Id)
    {
        return $this->db->get_where('OrderItem',array('Id'=>$Id))->row_array();
    }
        
    /*
     * Get all orderitem
     */
    function get_all_orderitem()
    {
        $this->db->order_by('Id', 'desc');
        return $this->db->get('OrderItem')->result_array();
    }
        
    /*
     * function to add new orderitem
     */
    function add_orderitem($params)
    {
        $this->db->insert('OrderItem',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update orderitem
     */
    function update_orderitem($Id,$params)
    {
        $this->db->where('Id',$Id);
        return $this->db->update('OrderItem',$params);
    }
    
    /*
     * function to delete orderitem
     */
    function delete_orderitem($Id)
    {
        return $this->db->delete('OrderItem',array('Id'=>$Id));
    }
}
