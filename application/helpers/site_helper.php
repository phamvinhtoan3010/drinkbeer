
<?php 


if(!function_exists('convertDatetime'))
{
	function convertDatetime($datetime)
	{
		$arr_Datetime = explode(" ",$datetime);
		$arr_day = explode("/",$arr_Datetime[0]);
		return ($arr_day[2]."-".$arr_day[1]."-".$arr_day[0]." ".$arr_Datetime[1]);
	}
}
if(!function_exists('toTimeString'))
{
	function toTimeString($ptime)
	{
		$sum_time_now = strtotime($ptime);
		$date = date("j/m/Y", mktime(0, 0, 0, date("m", $sum_time_now), date("d", $sum_time_now), date("Y", $sum_time_now)));
		$time = date("h:ia", mktime(date("h", $sum_time_now),date("i", $sum_time_now),date("s", $sum_time_now),0, 0, 0));	
		return $date . ' at ' . $time;
	}
}




?>