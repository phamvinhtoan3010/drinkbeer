<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Order extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Order_model');
        $this->load->helper('site_helper');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->load->helper(array('form', 'url', 'date'));
        session_start();
    } 

    /*
     * Listing of order
     */
    function index()
    {
        $data['order'] = $this->Order_model->get_all_order();
        
        $data['_view'] = 'order/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new order
     */
    function add()
    {   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('TableId','TableId','required');
		$this->form_validation->set_rules('Price','Price','required|integer');
		$this->form_validation->set_rules('Status','Status','required');
		
		if($this->form_validation->run())     
        {   
            $params = array(
				'TableId' => $this->input->post('TableId'),
				'Status' => $this->input->post('Status'),
				'Price' => $this->input->post('Price'),
				'CreateDate' => $this->input->post('CreateDate'),
            );
            
            $order_id = $this->Order_model->add_order($params);
            redirect('order/index');
        }
        else
        {            
            $data['_view'] = 'order/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a order
     */
    function edit($Id)
    {   
        // check if the order exists before trying to edit it
        $data['order'] = $this->Order_model->get_order($Id);
        
        if(isset($data['order']['Id']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('TableId','TableId','required');
			$this->form_validation->set_rules('Price','Price','required|integer');
			$this->form_validation->set_rules('Status','Status','required');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'TableId' => $this->input->post('TableId'),
					'Status' => $this->input->post('Status'),
					'Price' => $this->input->post('Price'),
					'CreateDate' => $this->input->post('CreateDate'),
                );

                $this->Order_model->update_order($Id,$params);            
                redirect('order/index');
            }
            else
            {
                $data['_view'] = 'order/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The order you are trying to edit does not exist.');
    } 

    /*
     * Deleting order
     */
    function remove($Id)
    {
        $order = $this->Order_model->get_order($Id);

        // check if the order exists before trying to delete it
        if(isset($order['Id']))
        {
            $this->Order_model->delete_order($Id);
            redirect('order/index');
        }
        else
            show_error('The order you are trying to delete does not exist.');
    }
    
}
