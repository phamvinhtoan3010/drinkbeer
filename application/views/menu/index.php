<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Menu Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('menu/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>IsActive</th>
						<th>Type</th>
						<th>Name</th>
						<th>Code</th>
						<th>Price</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($menu as $M){ ?>
                    <tr>
						<td><?php echo $M['Id']; ?></td>
						<td><?php echo $M['IsActive']; ?></td>
						<td><?php echo $M['Type']; ?></td>
						<td><?php echo $M['Name']; ?></td>
						<td><?php echo $M['Code']; ?></td>
						<td><?php echo $M['Price']; ?></td>
						<td>
                            <a href="<?php echo site_url('menu/edit/'.$M['Id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('menu/remove/'.$M['Id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
