<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Menu Edit</h3>
            </div>
			<?php echo form_open('menu/edit/'.$menu['Id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="IsActive" value="1" <?php echo ($menu['IsActive']==1 ? 'checked="checked"' : ''); ?> id='IsActive' />
							<label for="IsActive" class="control-label"><span class="text-danger">*</span>IsActive</label>
							<span class="text-danger"><?php echo form_error('IsActive');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Type" class="control-label"><span class="text-danger">*</span>Type</label>
						<div class="form-group">
							<select name="Type" class="form-control">
								<option value="">select</option>
								<?php 
								$Type_values = array(
									'1'=>'monchinh',
									'2'=>'monphu',
									'3'=>'trangmieng',
								);

								foreach($Type_values as $value => $display_text)
								{
									$selected = ($value == $menu['Type']) ? ' selected="selected"' : "";

									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('Type');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Name" class="control-label"><span class="text-danger">*</span>Name</label>
						<div class="form-group">
							<input type="text" name="Name" value="<?php echo ($this->input->post('Name') ? $this->input->post('Name') : $menu['Name']); ?>" class="form-control" id="Name" />
							<span class="text-danger"><?php echo form_error('Name');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Code" class="control-label"><span class="text-danger">*</span>Code</label>
						<div class="form-group">
							<input type="text" name="Code" value="<?php echo ($this->input->post('Code') ? $this->input->post('Code') : $menu['Code']); ?>" class="form-control" id="Code" />
							<span class="text-danger"><?php echo form_error('Code');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Price" class="control-label"><span class="text-danger">*</span>Price</label>
						<div class="form-group">
							<input type="text" name="Price" value="<?php echo ($this->input->post('Price') ? $this->input->post('Price') : $menu['Price']); ?>" class="form-control" id="Price" />
							<span class="text-danger"><?php echo form_error('Price');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>