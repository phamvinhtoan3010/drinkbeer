<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Costspend Add</h3>
            </div>
            <?php echo form_open('costspend/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="Name" class="control-label"><span class="text-danger">*</span>Name</label>
						<div class="form-group">
							<input type="text" name="Name" value="<?php echo $this->input->post('Name'); ?>" class="form-control" id="Name" />
							<span class="text-danger"><?php echo form_error('Name');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Status" class="control-label"><span class="text-danger">*</span>Status</label>
						<div class="form-group">
							<input type="text" name="Status" value="<?php echo $this->input->post('Status'); ?>" class="form-control" id="Status" />
							<span class="text-danger"><?php echo form_error('Status');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="CreateDate" class="control-label">CreateDate</label>
						<div class="form-group">
							<input type="date" name="CreateDate" value="<?php echo $this->input->post('CreateDate'); ?>" class="form-control" id="CreateDate" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="SpendDate" class="control-label"><span class="text-danger">*</span>SpendDate</label>
						<div class="form-group">
							<input type="date" name="SpendDate" value="<?php echo $this->input->post('SpendDate'); ?>" class="form-control" id="SpendDate" />
							<span class="text-danger"><?php echo form_error('SpendDate');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Price" class="control-label"><span class="text-danger">*</span>Price</label>
						<div class="form-group">
							<input type="number" name="Price" value="<?php echo $this->input->post('Price'); ?>" class="form-control" id="Price" />
							<span class="text-danger"><?php echo form_error('Price');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Description" class="control-label">Description</label>
						<div class="form-group">
							<textarea name="Description" class="form-control" id="Description"><?php echo $this->input->post('Description'); ?></textarea>
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>
<script>


</script>