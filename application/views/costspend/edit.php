<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Costspend Edit</h3>
            </div>
			<?php echo form_open('costspend/edit/'.$costspend['Id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="Name" class="control-label"><span class="text-danger">*</span>Name</label>
						<div class="form-group">
							<input type="text" name="Name" value="<?php echo ($this->input->post('Name') ? $this->input->post('Name') : $costspend['Name']); ?>" class="form-control" id="Name" />
							<span class="text-danger"><?php echo form_error('Name');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Status" class="control-label"><span class="text-danger">*</span>Status</label>
						<div class="form-group">
							<input type="text" name="Status" value="<?php echo ($this->input->post('Status') ? $this->input->post('Status') : $costspend['Status']); ?>" class="form-control" id="Status" />
							<span class="text-danger"><?php echo form_error('Status');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="CreateDate" class="control-label">CreateDate</label>
						<div class="form-group">
							<input type="text" name="CreateDate" value="<?php echo ($this->input->post('CreateDate') ? $this->input->post('CreateDate') : $costspend['CreateDate']); ?>" class="form-control" id="CreateDate" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="SpendDate" class="control-label"><span class="text-danger">*</span>SpendDate</label>
						<div class="form-group">
							<input type="text" name="SpendDate" value="<?php echo ($this->input->post('SpendDate') ? $this->input->post('SpendDate') : $costspend['SpendDate']); ?>" class="form-control" id="SpendDate" />
							<span class="text-danger"><?php echo form_error('SpendDate');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Price" class="control-label"><span class="text-danger">*</span>Price</label>
						<div class="form-group">
							<input type="text" name="Price" value="<?php echo ($this->input->post('Price') ? $this->input->post('Price') : $costspend['Price']); ?>" class="form-control" id="Price" />
							<span class="text-danger"><?php echo form_error('Price');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Description" class="control-label">Description</label>
						<div class="form-group">
							<textarea name="Description" class="form-control" id="Description"><?php echo ($this->input->post('Description') ? $this->input->post('Description') : $costspend['Description']); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>