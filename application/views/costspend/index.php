<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Costspend Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('costspend/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>Name</th>
						<th>Status</th>
						<th>CreateDate</th>
						<th>SpendDate</th>
						<th>Price</th>
						<th>Description</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($costspend as $C){ ?>
                    <tr>
						<td><?php echo $C['Id']; ?></td>
						<td><?php echo $C['Name']; ?></td>
						<td><?php echo $C['Status']; ?></td>
						<td><?php echo $C['CreateDate']; ?></td>
						<td><?php echo $C['SpendDate']; ?></td>
						<td><?php echo $C['Price']; ?></td>
						<td><?php echo $C['Description']; ?></td>
						<td>
                            <a href="<?php echo site_url('costspend/edit/'.$C['Id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('costspend/remove/'.$C['Id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
