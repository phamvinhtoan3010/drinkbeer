<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Typemenu Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('typemenu/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($typemenu as $T){ ?>
                    <tr>
						<td><?php echo $T['Id']; ?></td>
						<td><?php echo $T['Name']; ?></td>
						<td><?php echo $T['Description']; ?></td>
						<td>
                            <a href="<?php echo site_url('typemenu/edit/'.$T['Id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('typemenu/remove/'.$T['Id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
