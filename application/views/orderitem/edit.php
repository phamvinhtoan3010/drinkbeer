<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Orderitem Edit</h3>
            </div>
			<?php echo form_open('orderitem/edit/'.$orderitem['Id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="HasDone" value="1" <?php echo ($orderitem['HasDone']==1 ? 'checked="checked"' : ''); ?> id='HasDone' />
							<label for="HasDone" class="control-label"><span class="text-danger">*</span>HasDone</label>
							<span class="text-danger"><?php echo form_error('HasDone');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="MenuId" class="control-label"><span class="text-danger">*</span>MenuId</label>
						<div class="form-group">
							<select name="MenuId" class="form-control">
								<option value="">select</option>
								<?php 
								$MenuId_values = array(
									'1'=>'ga',
									'2'=>'bo',
									'3'=>'bia',
								);

								foreach($MenuId_values as $value => $display_text)
								{
									$selected = ($value == $orderitem['MenuId']) ? ' selected="selected"' : "";

									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('MenuId');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="OrderId" class="control-label">OrderId</label>
						<div class="form-group">
							<select name="OrderId" class="form-control">
								<option value="">select</option>
								<?php 
								$OrderId_values = array(
								);

								foreach($OrderId_values as $value => $display_text)
								{
									$selected = ($value == $orderitem['OrderId']) ? ' selected="selected"' : "";

									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Price" class="control-label"><span class="text-danger">*</span>Price</label>
						<div class="form-group">
							<input type="text" name="Price" value="<?php echo ($this->input->post('Price') ? $this->input->post('Price') : $orderitem['Price']); ?>" class="form-control" id="Price" />
							<span class="text-danger"><?php echo form_error('Price');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Quality" class="control-label"><span class="text-danger">*</span>Quality</label>
						<div class="form-group">
							<input type="text" name="Quality" value="<?php echo ($this->input->post('Quality') ? $this->input->post('Quality') : $orderitem['Quality']); ?>" class="form-control" id="Quality" />
							<span class="text-danger"><?php echo form_error('Quality');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>