<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Orderitem Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('orderitem/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>HasDone</th>
						<th>MenuId</th>
						<th>OrderId</th>
						<th>Price</th>
						<th>Quality</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($orderitem as $O){ ?>
                    <tr>
						<td><?php echo $O['Id']; ?></td>
						<td><?php echo $O['HasDone']; ?></td>
						<td><?php echo $O['MenuId']; ?></td>
						<td><?php echo $O['OrderId']; ?></td>
						<td><?php echo $O['Price']; ?></td>
						<td><?php echo $O['Quality']; ?></td>
						<td>
                            <a href="<?php echo site_url('orderitem/edit/'.$O['Id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('orderitem/remove/'.$O['Id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
