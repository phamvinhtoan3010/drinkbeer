<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Table Edit</h3>
            </div>
			<?php echo form_open('table/edit/'.$table['Id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="IsActive" value="1" <?php echo ($table['IsActive']==1 ? 'checked="checked"' : ''); ?> id='IsActive' />
							<label for="IsActive" class="control-label"><span class="text-danger">*</span>IsActive</label>
							<span class="text-danger"><?php echo form_error('IsActive');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Name" class="control-label"><span class="text-danger">*</span>Name</label>
						<div class="form-group">
							<input type="text" name="Name" value="<?php echo ($this->input->post('Name') ? $this->input->post('Name') : $table['Name']); ?>" class="form-control" id="Name" />
							<span class="text-danger"><?php echo form_error('Name');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Code" class="control-label"><span class="text-danger">*</span>Code</label>
						<div class="form-group">
							<input type="text" name="Code" value="<?php echo ($this->input->post('Code') ? $this->input->post('Code') : $table['Code']); ?>" class="form-control" id="Code" />
							<span class="text-danger"><?php echo form_error('Code');?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>