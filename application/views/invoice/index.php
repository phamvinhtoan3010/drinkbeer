<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Invoice Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('invoice/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>OrderId</th>
						<th>CreateDate</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($invoice as $I){ ?>
                    <tr>
						<td><?php echo $I['Id']; ?></td>
						<td><?php echo $I['OrderId']; ?></td>
						<td><?php echo $I['CreateDate']; ?></td>
						<td>
                            <a href="<?php echo site_url('invoice/edit/'.$I['Id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('invoice/remove/'.$I['Id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
