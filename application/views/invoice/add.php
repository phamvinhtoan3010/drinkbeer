<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Invoice Add</h3>
            </div>
            <?php echo form_open('invoice/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="OrderId" class="control-label">OrderId</label>
						<div class="form-group">
							<input type="text" name="OrderId" value="<?php echo $this->input->post('OrderId'); ?>" class="form-control" id="OrderId" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="CreateDate" class="control-label">CreateDate</label>
						<div class="form-group">
							<input type="text" name="CreateDate" value="<?php echo $this->input->post('CreateDate'); ?>" class="form-control" id="CreateDate" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>