<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Status Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('statu/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>Type</th>
						<th>Name</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($status as $S){ ?>
                    <tr>
						<td><?php echo $S['Id']; ?></td>
						<td><?php echo $S['Type']; ?></td>
						<td><?php echo $S['Name']; ?></td>
						<td>
                            <a href="<?php echo site_url('statu/edit/'.$S['Id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('statu/remove/'.$S['Id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
