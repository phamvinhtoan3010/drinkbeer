<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Order Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('order/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>TableId</th>
						<th>Status</th>
						<th>Price</th>
						<th>CreateDate</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($order as $O){ ?>
                    <tr>
						<td><?php echo $O['Id']; ?></td>
						<td><?php echo $O['TableId']; ?></td>
						<td><?php echo $O['Status']; ?></td>
						<td><?php echo $O['Price']; ?></td>
						<td><?php echo $O['CreateDate']; ?></td>
						<td>
                            <a href="<?php echo site_url('order/edit/'.$O['Id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('order/remove/'.$O['Id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
