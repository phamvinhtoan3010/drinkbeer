<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Order Edit</h3>
            </div>
			<?php echo form_open('order/edit/'.$order['Id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="TableId" class="control-label"><span class="text-danger">*</span>TableId</label>
						<div class="form-group">
							<select name="TableId" class="form-control">
								<option value="">select</option>
								<?php 
								$TableId_values = array(
									'1'=>'ban1',
									'2'=>'ban2',
								);

								foreach($TableId_values as $value => $display_text)
								{
									$selected = ($value == $order['TableId']) ? ' selected="selected"' : "";

									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('TableId');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Status" class="control-label"><span class="text-danger">*</span>Status</label>
						<div class="form-group">
							<select name="Status" class="form-control">
								<option value="">select</option>
								<?php 
								$Status_values = array(
									'1'=>'moi',
									'2'=>'da hoan thanh',
									'3'=>'huy',
								);

								foreach($Status_values as $value => $display_text)
								{
									$selected = ($value == $order['Status']) ? ' selected="selected"' : "";

									echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
								} 
								?>
							</select>
							<span class="text-danger"><?php echo form_error('Status');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Price" class="control-label"><span class="text-danger">*</span>Price</label>
						<div class="form-group">
							<input type="text" name="Price" value="<?php echo ($this->input->post('Price') ? $this->input->post('Price') : $order['Price']); ?>" class="form-control" id="Price" />
							<span class="text-danger"><?php echo form_error('Price');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="CreateDate" class="control-label">CreateDate</label>
						<div class="form-group">
							<input type="text" name="CreateDate" value="<?php echo ($this->input->post('CreateDate') ? $this->input->post('CreateDate') : $order['CreateDate']); ?>" class="form-control" id="CreateDate" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>